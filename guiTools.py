from Tkinter import *
import time
import tkMessageBox
#from PIL import Image, ImageTk
tconstant = 0.11

class Ticker(object):

    def __init__(self, size, container):
        self.labels = []
        self.ltext = []
        for i in range(0, size):
            self.ltext.append(StringVar())
            self.labels.append(Label(container, 
                textvariable=self.ltext[i],
                bd="1", 
                font=("Calibri", 12), 
                relief = 'groove', 
                background = 'grey',
                fg = 'white'))
            self.labels[i].pack(side=LEFT)
            self.ltext[i].set("P8: 888888")
        self.labels[0].config(bg='white',fg="black")
        
    def UpdateTicker(self,line):
        for i in range(1,len(self.ltext)):
            x = len(self.ltext) - i
            s = self.ltext[x-1].get()
            self.ltext[x].set(s)
            self.labels[i].config(bg='darkgrey')
            self.labels[i].update()
            time.sleep(tconstant)
            self.labels[i].config(bg='grey')
            self.labels[i].update()
        self.ltext[0].set(line)


class Disk(object):

    def __init__(self, container):
        self.labeltext = StringVar()
        self.label=Label(container,
                textvariable=self.labeltext,
                bd="1",
                font=("Calibri",12),
                bg='grey',
                fg='white')
        self.labeltext.set('Disk')
        self.pages = []

        for x in range(0,16):
            for y in range(0,64):
                self.pages.append(Label(container,
                    activebackground='white',
                    background='grey',
                    bd=1,
                    relief=GROOVE))
                self.pages[len(self.pages)-1].grid(row=x,column=y)
        self.label.grid(row=16,columnspan=64)

class Control(object):

    def __init__(self,container,funcStep,funcEnd,funcpause):
        self.stepimage = PhotoImage(file='stepPlay.gif')
        self.step = Button(container, 
                image=self.stepimage,
                command=funcStep)
        
        self.endimage = PhotoImage(file='endPlay.gif')
        self.end = Button(container,
                image=self.endimage,
                command=funcEnd)
       
        self.pauseONimage = PhotoImage(file='pauseON.gif')
        self.pauseOFFimage = PhotoImage(file='pauseOFF.gif')
        self.pause = Button(container,
                image=self.pauseOFFimage,
                command=funcpause)
        self.step.grid(row=0)
        self.end.grid(row=0,column=1)
        self.pause.grid(row=0,column=2)

class WorkingSet(object):

    command = None

    def __init__(self,container,wsfunc):
        global command 
        command = wsfunc
        self.processes = {}
        self.container = container
        self.label = Label(self.container,
                text = "Encountered Processes",
                bd = '1',
                font = ('Calibri',12),
                bg='grey',
                fg='white')
        self.label.pack()
    

    def UpdateWS(self,processes):
        
        def pressed(pid):
            command(int(pid[5]))

        # add any new processes to working set
        for x in processes:
            pids = processes[x][0].pltext.get()
            if not (pids in self.processes):
                self.processes[pids] = Button(self.container,
                        command=lambda:pressed(pids),
                        bd='2',
                        relief=GROOVE,
                        text = pids)
                self.processes[pids].pack()


class Pcb(object):

    def __init__(self, processid, container, func):
        self.pages = [] # button pages
        self.pagelabelVar = [] # stringVar for button 
        self.pagelabels = [] # info list form for each button
                # order of list: [page num, mem addr, tru]
        self.pltext = StringVar() # pid label txt
        self.plabel = Label(container, # pid label
                textvariable=self.pltext,
                bd='1',
                font=('Calibri',12),
                bg='grey',
                fg='white')
        self.plabel.grid(row=0,)
        self.pltext.set("Pid: "+str(processid))
        self.pcblabel= Label(container,
                text='PCB',
                bd='1',
                font=('Calibri',12),
                bg='grey',
                fg='white')
        for y in range(1,5):
            for x in range(1,17):
                i =  (((y-1)*16)+x)-1
                # list [pgnum, mem addr,tru]
                self.pagelabels.append(list((str(hex(i)),'NA','NA')))
                string = ' : '.join(self.pagelabels[i])
                self.pagelabelVar.append(StringVar()) 
                self.pagelabelVar[i].set(string)
                self.pages.append(Button(container,
                    command=lambda:func(self.pltext.get(),self.pagelabels[i][0]),
                    bd='2',
                    relief=GROOVE,
                    textvariable = self.pagelabelVar[i]))
                self.pages[len(self.pages)-1].grid(row=x,column=y)
        self.pcblabel.grid(row=17,columnspan=16)



    def UpdatePcb(self,pnumber,memaddr,t):
        x = pnumber 
        # update page label list info
        self.pagelabels[x][1] = str(hex(memaddr))
        self.pagelabels[x][2] = str(t)
        # update page string var from page label
        self.pagelabelVar[x].set(' : '.join(self.pagelabels[x]))
        # flash the button
        self.pages[x].config(bg='white')
        self.pages[x].update()
        time.sleep(tconstant)
        self.pages[x].config(bg='grey')
        self.pages[x].update()
        
class Memory(object):

    def __init__(self,container):
        self.labelText=StringVar()
        self.labelText.set('Memory')
        self.label = Label(container, # pid label
                textvariable=self.labelText,
                bd='1',
                font=('Calibri',12),
                bg='grey',
                fg='white')
 
        self.memseg = [] # the button
        self.memsegtxt = [] # the string var of the button
        self.memseginfo = [] # the string in list form
           # [address:pid:pg:tru] order of memseginfo 

        for x in range(0,16):
            self.memsegtxt.append(StringVar()) 
            self.memseg.append(Label(container,
                    textvariable=self.memsegtxt[x],
                    bd='1',
                    font=('Calibri',12),
                    bg='grey',
                    fg='white',))
            #'address:pid:pg#:tru' order of memseginfo 
            self.memseginfo.append(list((str(hex(x)),'N/A','   N/A','N/A')))
            txt = str(' : '.join(self.memseginfo[x]))
            self.memsegtxt[x].set(txt)
            self.memseg[x].pack()

        self.label.pack()

    def flashMemory(self,pid,page):
        found = False
        print 'original page: '+ str(page)
        page = 63 - page
        print '63-page: '+str(page)
        page = format(page,'06b')
        for x in range(0,16):
            spid = self.memseginfo[x][1]
            spage = self.memseginfo[x][2]
            print 'pid ' + pid
            print 'spid ' +spid
            print 'page '+ str(page)
            print 'spage ' + str(spage)
            if spid == pid and spage == page:
                # this processes's page exists in memory
                # flash the spot
                self.memseg[x].config(bg='white',fg='black')
                self.memseg[x].update()
                time.sleep(tconstant)
                self.memseg[x].config(bg='grey',fg='white')
                self.memseg[x].update()
                found = True
        if not found:
            tkMessageBox.showinfo("page detail", "Page does not exist in Memory")


    def UpdateMemory(self,pcb,pnumber,t):
        pid = pcb.pltext.get() # string pid 
        alreadyhere = False
        # highlight free frames
        freeframes = []
        # accessed for present page access only updates tru
        accessed=[self.memseg[0],self.memsegtxt[0],self.memseginfo[0],0]  
        for x in range(0,16):
            pnumberx = self.memseginfo[x][2]
            pidx =  self.memseginfo[x][1]
            if pnumberx == pnumber and pidx == pid:
                alreadyhere = True
                accessed[0] = self.memseg[x]
                accessed[1] = self.memsegtxt[x]
                accessed[2] = self.memseginfo[x]
                accessed[3] = x
            if self.memseginfo[x][1] == 'N/A':
                self.memseg[x].config(bg='yellow',fg='black')
                self.memseg[x].update()
                time.sleep(tconstant)
                f = [self.memseg[x],self.memsegtxt[x],self.memseginfo[x],x] 
                freeframes.append(f)

        if not alreadyhere:
            # select victim page arbitrary first assignment
            #victim=[self.memseg[0],self.memsegtxt[0],self.memseginfo[0],0]  
            victim = ['','','','']

            if len(freeframes) == 0:
                # no free frames find real victim
                lru = t # set it to something that will get replaced
                
                for x in range(0,16):

                    if int(self.memseginfo[x][3])<lru:
                        #reset prev victim colors
                        if victim[0] != '':
                            victim[0].config(bg='grey',fg='white')
                            victim[0].update()
                        # set up new victim
                        lru = int(self.memseginfo[x][3])
                        victim[0] = self.memseg[x]
                        victim[1] = self.memsegtxt[x]
                        victim[2] = self.memseginfo[x]
                        victim[3] = x
                        victim[0].config(bg='red',fg='Black')
                        victim[0].update()
                        time.sleep(tconstant)
                     
                    # focus memseg(i.e. frame) 
                    if self.memseg[x] != victim[0]:
                        self.memseg[x].config(bg='blue',fg='white') 
                        self.memseg[x].update()
                        time.sleep(tconstant)

                    #reset colors if not victim
                    for i in range(0,16):
                        if self.memseg[i] != victim[0]:
                            self.memseg[i].config(bg='grey',fg='white')
                            self.memseg[i].update()

            else:
                # free frame available make that the victim
                victim = freeframes.pop(0)
                victim[0].config(fg='black',bg='blue')
                victim[0].update()
                time.sleep(tconstant)
                victim[0].config(bg='grey')
                time.sleep(tconstant)
                victim[0].update()
                # remove from free frames

           
            # insert new page
            victim[2][1] = pid
            victim[2][2] = pnumber
            victim[2][3] = str(t)
            string = " : ".join(victim[2])
            victim[1].set(string) #msinfo to mstext
            victim[0].update()
            #flash white for notification
            victim[0].config(bg='white',fg='black')
            victim[0].update()
            time.sleep(tconstant)
            # reset victim's colors
            victim[0].config(bg='grey',fg='white')
            victim[0].update()
            # return memory address for pcb
            return victim[3]     
        else:
            #update Time Recently Used (tru)
            accessed[2][3] = str(t)
            string = " : ".join(accessed[2])
            accessed[1].set(string)
            # flash label
            accessed[0].config(bg='white')
            accessed[0].update()
            time.sleep(tconstant)
            accessed[0].config(bg='grey')
            accessed[0].update()
            # return memory address for pcb
            return accessed[3]     


