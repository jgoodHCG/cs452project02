from Tkinter import * 
from guiTools import *

TICKER_TAPE_SIZE = 8

root = Tk()           


colorstring = '#272422'
window = Frame(root,
        bg=colorstring)
window.grid()

processes = {}

f = open(sys.argv[1])
t = 0
ispause = False

def processInput(l):
    l = l.split('\t')
    l[1] = l[1][0:6]
    s = l[1]
    p = int(l[1], 2)
    l = ' '.join(l)
    lps = (l,p,s)
    return lps

def step():
    #sandwich command config
    #because w/o it, repeated pressings
    #creates out of order results
    # from repeated immediate step() calls
    con.step.config(command=stoppressing)
    line = f.readline()
    global t 
    t += 1
    s = processInput(line) # tuple (line,page)
    pgstr = s[2] 
    pgint = s[1]
    tt.UpdateTicker(s[0])
    pid = int(line[1])
    
    global curpcb
    prevpcb = curpcb

    if not(pid in processes):
        pc = Frame(pcbc)
        processes[pid]=[Pcb(pid,pc,pcbfunc),pc]
        pc.grid()

    
    prevpcb[1].grid_forget()
    curpcb = processes[pid]
    curpcb[1].grid()
    ws.UpdateWS(processes)
    memaddr=mem.UpdateMemory(curpcb[0],pgstr,t)
    curpcb[0].UpdatePcb(pgint,memaddr,t)
    pcbc.update()
    con.step.config(command=step) 

def end():
    con.end.config(command=stoppressing)
    lnNum = linenumber(sys.argv[1])
    while lnNum > 0:
        step()
        lnNum -= 1
        if ispause:
            break
    con.end.config(command=end)

def pause():
    global ispause
    global con
    global concon
    if ispause:
        ispause = False
        con.pause.config(
                image = con.pauseOFFimage)
        concon.update()
    else:
        ispause = True
        con.pause.config(
                image =con.pauseONimage)
        concon.update()
        
def stoppressing():
    print 'stop pressing that for a moment'

def wsFunc(pid):
    global curpcb
    prevpcb = curpcb
    prevpcb[1].grid_forget()
    newpcb = processes[pid]
    curpcb = newpcb
    curpcb[1].grid()

def pcbfunc(pid,page):
    mem.flashMemory(pid,page)

def linenumber(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i+1

tc = Frame(window)
tt = Ticker(TICKER_TAPE_SIZE, tc)
tc.grid(column=0,
        columnspan=9,
        pady='10')

pcbc = Frame(window,
        height='540',
        width='400')
pc = Frame(pcbc)
curpcb = [Pcb(-1,pc,pcbfunc),pc]
pcbc.grid(row=2,
        column=1,
        columnspan=4,
        pady='10')

concon = Frame(window)
con = Control(concon,step,end,pause)
concon.grid(row=4,
        column=4, 
        columnspan=3)

memcon = Frame(window)
mem = Memory(memcon)
memcon.grid(row=2, 
        column=7, 
        columnspan=2,
        padx = '20')

workingsetcon = Frame(window)
ws = WorkingSet(workingsetcon,wsFunc)
workingsetcon.grid(column=0,
        padx='20',
        row = 2)
#dc = Frame(window)
#disk = Disk(dc)
#dc.grid(row=2,column=0,rowspan=3,columnspan=3)
root.mainloop()      
